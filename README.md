### 项目介绍
* 本项目基于微信小程序云开发，实现零成本的数字货币量化交易系统。
* 项目量化交易策略为网格交易策略，可参考本项目自行开发其他策略。
* 本项目提供一种思路：即利用微信小程序云开发的免费资源实现零成本的交易系统，同时使用微信小程序作为前端展示，更方便查看到交易系统的实时状态。
* 免费的原理基于微信小程序云开发每个月的免费额度，相当于免费的云服务器资源。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/112018_1ada78e3_7494348.png "屏幕截图.png")


### 产品截图

![小程序](https://images.gitee.com/uploads/images/2020/0802/105517_b99b4570_7494348.png "屏幕截图.png")


### 系统结构
![系统结构](https://images.gitee.com/uploads/images/2020/0802/105429_c662af75_7494348.png "屏幕截图.png")

### 网格交易策略
1. 使用云函数的定时触发器，设置为每三分钟触发检测一次市价。
2. 策略逻辑流程图：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/110854_0a05d8ea_7494348.png "屏幕截图.png")


### 项目功能
* 网格交易策略
  * 支持交易对设置
  * 支持网格数量设置
  * 支持网格大小设置
* 支持交易通知提醒
* 小程序查看账户状态、网格信息、交易记录、网格列表
* 支持交易所：[中币](https://www.zbex3.com/cn/register?recommendCode=hj0d0e)（只写了中币的API，没有账号可点击进入注册，其他交易所请自行开发）


### 使用说明
==本项目基于微信小程序开发，请自备相关基础支持。==
##### 微信小程序开发环境搭建
1. 申请账号&开发工具：

* 按照[官方说明](https://developers.weixin.qq.com/miniprogram/dev/framework/quickstart/getstart.html#%E7%94%B3%E8%AF%B7%E5%B8%90%E5%8F%B7)注册一个个人用户账号和安装开发环境。
* 填写小程序信息（服务类目选择‘工具’-‘信息查询’）

* 开通订阅消息
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/111028_9ef4d305_7494348.png "屏幕截图.png")
在“功能”-“订阅消息”-“公共模版库”，搜索“订单待处理提醒”，选用该模版。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/111058_d45475b4_7494348.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/111119_131ab92b_7494348.png "屏幕截图.png")

2. 下载本项目代码

```
git clone https://gitee.com/code_cow/quantRobot.git
```

3. 导入项目
* 打开IDE，选择导入项目
* 选择项目路径，输入APPID
* 确定
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/111235_ce5130e5_7494348.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/111310_2a167ec3_7494348.png "屏幕截图.png")

4. 开通‘云开发’
* 点击云开发，选择开通
* 设置环境参数
* 确定开通
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/111337_ed498e13_7494348.png "屏幕截图.png")

5. 配置云数据库

###### account集合
* 创建“account”集合
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/111413_1690d8bd_7494348.png "屏幕截图.png")

* 添加1条记录
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/111432_1af72b58_7494348.png "屏幕截图.png")

* 权限设置为“所有用户可读，仅创建者可读写”

###### trade集合
* 创建“trade”集合
* 权限设置为“所有用户可读，仅创建者可读写”

6. 前端配置
* 设置account记录的ID（步骤5中创建的account记录ID）
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/111659_c69dde3a_7494348.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/111607_5552a5c4_7494348.png "屏幕截图.png")
* 订阅消息模版Id设置（步骤1中模版消息的ID）
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/111813_2073a8fb_7494348.png "屏幕截图.png")

7. 配置云函数参数
* 交易参数：见下文交易参数配置
* 数据库参数：集合account中创建的记录ID
* 中币ZB的参数设置：注册[中币](https://www.zbex3.com/cn/register?recommendCode=hj0d0e)后，进入API设置，创建API密钥
* 通知ID
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/111917_fe91a3e1_7494348.png "屏幕截图.png")

8. 上传云函数
* 右击functions-Trigger
* 选择“上传并部署，云端安装依赖”
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/112156_a7f8b1cc_7494348.png "屏幕截图.png")
* 后台查看到函数已部署


9. 配置云函数超时时间到60秒

10. 上传触发器（系统开始运行）
* 右击functions-Trigger
* 选择上传触发器
* 3分钟后云函数触发器开始运行，可在后台查看运行日志
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/112532_addfe1d9_7494348.png "屏幕截图.png")
ps. 可自行修改触发器间隔，如果频率过高，可能会超过每月免费额度。

11. 编译小程序
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/112616_cd7fe8a3_7494348.png "屏幕截图.png")

### 问题汇总
如使用中有相关问题，可提交issue。或加微信qzhurui，备注量化交易。
1. 为什么要开源？
作者最早在[FMZ量化](https://www.fmz.com/sign-up/2207643)平台编写量化策略，得益于该平台强大的回测功能，在行情好的情况下已经获利颇丰，为了回馈社区，我也把部分策略开源在FMZ平台，因此也认识了不少同行好友，期间他们反馈没有稳定的服务器，恰好我对小程序有所研究，正好一结合实现了零成本的量化交易系统，朋友希望我写个说明教教他们，索性直接开源出来供大家一起发挥。


### 请我喝咖啡
如果该项目对你有所帮助，可以请我喝杯咖啡。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/121018_37922367_7494348.png "屏幕截图.png")